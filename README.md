# Node.js SMS Flow Testing with Twilio

This application helps the SMS team test out new flows and how they appear on on different devices. 

# Dependencies

The modules used are: body-parser ejs express request twilio

# Contact/Question

Contact [andrew.schweinfurth@walgreens.com](mailto:andrew.schweinfurth@walgreens.com) for any questions or comments.
