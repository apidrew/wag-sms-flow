var request = require('request'),
	bodyParser = require('body-parser'),
	express = require('express'),
	app = express(),
	Datastore = require('nedb'),
	twilio = require('twilio'),
	twilio_client = twilio('AC21cdabd03acdde0ad88b0699fa9fe98c', '44cae2eca44ffbb5e0c6bbba4bf51a7a');

app.set('port', (process.env.PORT || 3000));
app.use(express.static(__dirname + '/public'));
app.set('views', __dirname + '/views');
app.set('view engine', 'ejs');
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

console.log('Connecting Local Database');
var db = {};
db.logs = new Datastore({ filename: __dirname+'/db/logs.db', autoload: true });
db.flows = new Datastore({ filename: __dirname+'/db/flows.db', autoload: true });
db.flows.ensureIndex({fieldName:"incoming",unique:true});
console.log('Local Databases Loaded');
console.log('Starting application');
app.listen(app.get('port'), function() 
{
	console.log('App is running, keep the ship steady on a heading to port: ', app.get('port'));
});

app.get('/',function(req,res)
{
	var opts = {title:"Flow List",flows:[]};
	FindFlows(function(flows)
	{
		opts.flows = flows.flows;
		res.render("pages/index",opts);
	});
});
app.get('/logs',function(req,res)
{
	var opts = {title:"Logs List",logs:[]};
	FindLogs(function(logs)
	{
		opts.logs = logs.logs;
		res.render("pages/logs",opts);
	});
});
app.post('/addFlow',function(req,res)
{
	res.setHeader('Access-Control-Allow-Origin', '*');
	var incoming = (req.body.incoming) ? req.body.incoming : "",
		outgoing = (req.body.outgoing) ? req.body.outgoing : "",
		d = new Date();
	if(incoming && incoming != "" && outgoing && outgoing != "")
	{
		console.log("Adding Flow: '"+incoming+"','"+outgoing+"',"+d.getTime());
		db.flows.insert({incoming:incoming,outgoing:outgoing,date:d.getTime()},function(err,result)
		{ 
			if(err)
			{ 
				LogBuilder("error","AddFlow","",incoming,err,function()
				{
					res.json({status:"error",json:err});
				});
			}
			else
			{
				LogBuilder("success","AddFlow","",incoming,{incoming:incoming,outgoing:outgoing},function()
				{
					res.json({status:"success",json:result.result.n});
				});
			}
		});
	}
	else
		res.json({status:"error",json:"Invalid Parameters passed in request!"});
});
app.post('/editFlow',function(req,res)
{
	res.setHeader('Access-Control-Allow-Origin', '*');
	var id = (req.body.id) ? req.body.id : "",
		incoming = (req.body.incoming) ? req.body.incoming : "",
		outgoing = (req.body.outgoing) ? req.body.outgoing : "",
		d = new Date();
		
	if(id && id != "" && incoming && incoming != "" && outgoing && outgoing != "")
	{
		console.log("Editing Flow: '"+incoming+"','"+outgoing+"',"+d.getTime());
		db.flows.update({'_id':id},{$set:{incoming:incoming,outgoing:outgoing,datetime:d.getTime()}},{multi:false,returnUpdatedDocs:true},function(err,numReplaced,result)
		{
			if(err)
			{ 
				LogBuilder("error","EditFlow","",id,err,function()
				{
					res.json({status:"error",json:err});
				});
			}
			else
			{
				LogBuilder("success","EditFlow","",id,{incoming:incoming,outgoing:outgoing},function()
				{
					res.json({status:"success",json:result});
				});
			}
		});
	}
	else
		res.json({status:"error",json:"Invalid Parameters passed in request!"});
});
app.post('/deleteFlow',function(req,res)
{
	res.setHeader('Access-Control-Allow-Origin', '*');
	var id = (req.body.id) ? req.body.id : "";
		
	if(id && id != "")
	{
		console.log("Deleting Flow: "+id);
		db.flows.remove({'_id':id},{},function(err,numRemoved)
		{
			if(err)
			{ 
				LogBuilder("error","DeleteFlow","",id,err,function()
				{
					res.json({status:"error",json:err});
				});
			}
			else
			{
				LogBuilder("success","DeleteFlow","",id,result.result.n+" Record Removed",function()
				{
					res.json({status:"success",json:numRemoved});
				});
			}
		});
	}
	else
		res.json({status:"error",json:"Invalid Parameters passed in request!"});
});
app.post('/send',function(req,res)
{
	var phoneNumber = (req.body.phoneNumber) ? req.body.phoneNumber : "",
		message = (req.body.message) ? req.body.message : "";
		
	if(phoneNumber && message)
	{
		ReplacePlaceholders(phoneNumber,message,function(response)
		{
			if(response.status == "success")
			{
				LogBuilder("success","TestFlows",phoneNumber,message,{number:phoneNumber,incoming:"TESTING_FLOW",outgoing:response.message},function()
				{					
					var opt = {
						to: phoneNumber,
						from: "2248013111",
						body: response.message
					};
					twilio_client.messages.create(opt,function(err, responseData) 
					{ 
						if (err) 
						{
							res.json({status:"error",message:err})
							console.log(err);
						} 
						else 
						{ 
							res.json({status:"success",from:responseData.from,to:phoneNumber,message:responseData.body});
						}
					});
				});
			}
			else
			{
				LogBuilder("critical","ReplacePlaceholders",phoneNumber,message,{number:phoneNumber,incoming:"TESTING_FLOW",outgoing:response.message},function()
				{
					var opt = {
						to: phoneNumber,
						from: "2248013111",
						body: response.message
					};
					twilio_client.messages.create(opt,function(err, responseData) 
					{ 
						if (err) 
						{
							res.json({status:"error",message:err})
							console.log(err);
						} 
						else 
						{ 
							res.json({status:"warning",from:responseData.from,to:phoneNumber,message:responseData.body});
						}
					});
				});
			}
		});
	}
	else
		res.json({status:"error",message:"phoneNumber or message not found in request!"});
});
app.all('/message',twilio.webhook({validate:false}), function(req, res) 
{
	var phoneNumber = (req.body.From) ? req.body.From.replace("+1","") : "",
		message = (req.body.Body) ? req.body.Body.toUpperCase().replace(/\s/g, '') : "",
		twiml = new twilio.twiml.MessagingResponse();
	
	if(phoneNumber && message)
	{
		FindFlows(function(flows)
		{
			var responseMsg = "",
				defaultResponseMsg = "";
			flows.flows.forEach(function(flow)
			{
				if(flow.incoming.toUpperCase() == message)
				{
					responseMsg = flow.outgoing;
				}
				if(flow.incoming.toUpperCase() == "DEFAULT")
				{
					defaultResponseMsg = flow.outgoing;
				}
			});
			if(responseMsg != "")
			{
				ReplacePlaceholders(phoneNumber,responseMsg,function(response)
				{
					if(response.status == "success")
					{
						LogBuilder("success","FindFlows",phoneNumber,message,{number:phoneNumber,incoming:message,outgoing:response.message},function()
						{
							twiml.message(response.message);
							res.send(twiml.toString());
						});
					}
					else
					{
						LogBuilder("critical","ReplacePlaceholders",phoneNumber,message,{number:phoneNumber,incoming:message,outgoing:response.message},function()
						{
							twiml.message(defaultResponseMsg);
							res.send(twiml.toString());
						});
					}
				}); 
			}
			else if(defaultResponseMsg != "")
			{
				LogBuilder("critical","FindFlows",phoneNumber,message,{number:phoneNumber,incoming:message,outgoing:defaultResponseMsg},function()
				{
					twiml.message(defaultResponseMsg);
					res.send(twiml.toString());
				});
			}
			else
			{
				LogBuilder("error","FindFlows",phoneNumber,message,{number:phoneNumber,incoming:message,outgoing:responseMsg},function()
				{
					twiml.message("Invalid message, please try again later");
					res.send(twiml.toString());
				});
			}
		});
	}
	else
	{
		LogBuilder("critical","Incoming Message",phoneNumber,message,"Invalid Request",function()
		{
			twiml.message("Error invalid format of request. Please pass phoneNumber and a keyword");
			res.send(twiml.toString());
		});
	}
});

var PhoneLookup = function(phoneNumber,callback)
{
	var options = {
		method: 'POST',
		url: 'https://services.walgreens.com/api/util/lytlookup',
		json: true,
		body: {
		   "apikey":"bnmO4urWsVHrV993STvaaL0uetpfvAPT",
		   "affId":"swoup",
		   "phoneNumber":phoneNumber
		}
	};
	console.log(options);
	request(options, function(err,response,body)
	{
		if(err)
			callback({status:"error",body:err});
		else if(body && body !== "")
		{
			if(body.matchProfiles && body.matchProfiles.length == 1)
				callback({status:"success",body:body.matchProfiles[0]});
			else
				callback({status:"warning",body:body.matchProfiles[0]});
		}
		else
			callback({status:"error",body:response});
	});
},
BRPointLookup = function(loyaltyMemberID,callback)
{
	var options = {
		method: 'POST',
		url: 'https://services.walgreens.com/api/inbox/widget/v1',
		json: true,
		body: {
			"loyaltyMemberId": loyaltyMemberID,
			"appver": "1.0",
			"apiKey": "d12ddc87a36f1cfb422dccb4ff0a7184",
			"version": "V1",
			"act": "homeScreenWidgets",
			"affId": "swoup"
		}
	};
	console.log(options);
	request(options, function(err,response,body)
	{
		if(err)
			callback({status:"error",body:err});
		else if(body && body !== "")
		{
			if(body.widgets && body.widgets.length > 0)
			{
				var points = "";
				body.widgets.forEach(function(widget)
				{
					if(widget.type == "BRHC" && widget.content && widget.content.loyaltypoints && widget.content.loyaltypoints.totalBRPoints)
					{
						points = widget.content.loyaltypoints.totalBRPoints;
					}
				});
				if(points != "")
					callback({status:"success",body:points});
				else
					callback({status:"error",body:"Unable to retrieve BRhc Points"});
			}
			else
			{
				callback({status:"error",body:"Unable to retrieve BRhc Points"});
			}
		}
		else
			callback({status:"error",body:"Unable to retrieve BRhc Points"});
	});
},
ReplacePlaceholders = function(phoneNumber,responseMsg,callback)
{
	PhoneLookup(phoneNumber,function(results)
	{
		console.log(results);
		responseMsg = responseMsg.replace(/{linebreak}/g, '\r\n');
		
		if(results.status == "success")
		{
			//TODO: More lookups like points, offers totals, and more.
			responseMsg = responseMsg.replace("{brid}",results.body.loyaltyMemberId);
			responseMsg = responseMsg.replace("{card}",results.body.loyaltyCardNumber);
			responseMsg = responseMsg.replace("{firstname}",results.body.firstName.toTitleCase());
			responseMsg = responseMsg.replace("{lastname}",results.body.lastName.toTitleCase());
			responseMsg = responseMsg.replace("{zipcode}",results.body.zipCode);
			responseMsg = responseMsg.replace("{email}",results.body.email);
			BRPointLookup(results.body.loyaltyMemberId,function(data)
			{
				if(data.status == "success")
				{
					responseMsg = responseMsg.replace("{points}",data.body);
					callback({status:"success",message:responseMsg});
				}
				else
				{
					responseMsg = responseMsg.replace("{points}","ERROR");
					callback({status:"success",message:responseMsg});
				}
			});
		}
		else if(results.status == "warning")
		{
			//TODO: More lookups like points, offers totals, and more.
			responseMsg = responseMsg.replace("{brid}",results.body.loyaltyMemberId);
			responseMsg = responseMsg.replace("{card}",results.body.loyaltyCardNumber);
			responseMsg = responseMsg.replace("{firstname}",results.body.firstName.toTitleCase());
			responseMsg = responseMsg.replace("{lastname}",results.body.lastName.toTitleCase());
			responseMsg = responseMsg.replace("{zipcode}",results.body.zipCode);
			responseMsg = responseMsg.replace("{email}",results.body.email);
			BRPointLookup(results.body.loyaltyMemberId,function(data)
			{
				if(data.status == "success")
				{
					responseMsg = responseMsg.replace("{brhcpoint}",results.body);
					callback({status:"success",message:responseMsg});
				}
				else
				{
					responseMsg = responseMsg.replace("{brhcpoint}","ERROR");
					callback({status:"success",message:responseMsg});
				}
			});
		}
		else
			callback({status:"error",message:responseMsg});
	});
},
FindFlows = function(callback)
{
	db.flows.find({},function(err,docs) 
	{
		if(err) 
		{
			console.log("Failed Getting Flows\n",err);
			callback({status:"error",flows:[]});
		}
		else
		{
			console.log("Found Flows:",docs.length);
			docs.sort(function compare(a, b) 
			{
				var dateA = new Date(a.datetime);
				var dateB = new Date(b.datetime);
				return dateB - dateA;
			});
			callback({status:"success",flows:docs});
		}
	});
},
FindLogs = function(callback)
{
	db.logs.find({},function(err,docs) 
	{
		if(err) 
		{
			console.log("Failed Getting Logs\n",err);
			callback({status:"error",logs:[]});
		}
		else
		{
			console.log("Found Logs:",docs.length);
			callback({status:"success",logs:docs});
		}
	});
},
LogBuilder = function(status,intent,number,message,data,callback)
{
	console.log("["+status.toUpperCase()+"] "+number+": "+message,{intent:intent,time:getCurrentTime(),data:data});
	db.logs.insert({
		status: status,
		intent: intent,
		time: getCurrentTime(),
		data: data
	},callback);
},
getCurrentTime = function()
{
	return (new Date).getTime().toString();
};

String.prototype.toTitleCase = function ()
{
    return this.replace(/\w\S*/g, function(txt){return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();});
};